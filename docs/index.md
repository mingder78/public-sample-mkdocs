# Welcome to BiMap

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

##  安哥, 直接改 /etc/mkdocs/docs 下的檔案

### index.md 為 首頁 markdown file
### 再把其他頁也加進 /etc/mkdocs/docs, 例如 test.md

### /etc/mkdocs/mkdocs.yml 裡有部分, 增加目錄的新連結, 例如 - Test: test.md 

```
pages:
    - Home: index.md
    - About: about.md
    - Code: code.md
    - Test: test.md
```

## images sample
### 若有圖檔 cat.jpg 為例, 可放在 docs/assets/images 下
/etc/mkdocs/docs/assets/images/cat.jpg

* 用以下來連結圖片
```
![cat](assets/images/cat.jpg)
```

![cat](assets/images/cat.jpg)

* 或連結遠端圖片的 URL 
```
Inline-style: 
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")
```


Inline-style: 
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")


## 其他 markdown 說明
* [mkDoc 詳細說明](http://www.mkdocs.org/user-guide/writing-your-docs/#writing-your-docs)
* [markdown guide](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
